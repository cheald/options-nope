import { blackScholes, solveIV } from "./blackScholes";
import { Env } from "./env";
import { Quote, YahooOptionResponse } from "./types";
import dayjs from "dayjs";
import { TradierHistoryInterval } from "@reycodev/tradier-client";
import { stdDev } from "./utils";
import axios, { AxiosResponse } from "axios";

const { prisma, riskFreeRate } = Env;

function midmarket(quote: Quote) {
  return (quote.ask + quote.bid) / 2;
}

export async function updateSymbol(symbol: string) {
  console.log("Updating symbol", symbol);
  await prisma.instrument.upsert({
    where: { symbol },
    create: { symbol, expirations_updated_at: new Date("Jan 1, 2000") },
    update: {},
    select: { expirations_updated_at: true },
  });
  await updateOption(symbol);
}

function solveOption(
  price: number,
  option: YahooOptionResponse,
  impliedVolatility: number | null = null
) {
  const mm = midmarket(option);
  const isCall = option.type === "call";
  let iv = impliedVolatility;
  const expiration = new Date(option.expiration * 1000 + 3600 * 1000 * 7);
  if (iv === null) {
    iv = solveIV(isCall, price, option.strike, riskFreeRate, expiration, mm);
  }

  iv = iv > 10 ? 10 : iv;
  const bs = blackScholes(
    isCall,
    price,
    option.strike,
    riskFreeRate,
    0,
    iv,
    expiration
  );

  const { gamma, delta, vega, rho, theta } = bs;
  return { gamma, delta, vega, rho, theta, iv };
}

async function getHistVolFor(symbol: string): Promise<number> {
  const volRes = await Env.prisma.$queryRaw`
  SELECT
    avg(volatility) as volatility
    FROM instrument_volatilities
    WHERE
      symbol = ${symbol}
    GROUP BY ending_at
    ORDER BY ending_at desc
    LIMIT 1;
`;

  if (volRes[0]) {
    const volatility = parseFloat(volRes[0].volatility);
    return volatility;
  } else {
    await updateHVEstimates(symbol, [7, 14, 30, 180]);
    return await getHistVolFor(symbol);
  }
}

async function updateOptionChain(
  symbol: string,
  price: number,
  expiration: Date,
  quotes: YahooOptionResponse[],
  batchId: Date,
  batchVolume: number
) {
  // const volatility: number = await getHistVolFor(symbol);
  for (let i = 0; i < quotes.length; i++) {
    const option = quotes[i];
    try {
      const { gamma, delta, vega, rho, theta, iv } = solveOption(price, option);

      const mostRecentOption = await prisma.option.findFirst({
        where: {
          name: option.contractSymbol,
          created_at: {
            gt: dayjs().startOf("day").toDate(),
          },
        },
        orderBy: {
          created_at: "desc",
        },
      });

      const expDate = dayjs(new Date(option.expiration * 1000)).utc();
      const payload = {
        symbol,
        expiration: expDate.format("YYYY-MM-DD"),
        expiration_date: expDate.toDate(),
        strike: option.strike,
        spot: price,
        name: option.contractSymbol,
        option_type: option.type,
        created_at: batchId,
        last: option.lastPrice,
        change: option.change,
        prev_volume: mostRecentOption?.volume || 0,
        underlying_volume: batchVolume,
        volume: option.volume || 0,
        bid: option.bid,
        ask: option.ask,
        last_volume: option.volume,
        open_interest: option.openInterest,
        gamma,
        delta,
        theta,
        rho,
        vega,
        implied_volatility: iv,
      };

      await prisma.option
        .create({
          data: payload,
        })
        .catch((e) => console.error(e, payload));
    } catch (e) {
      console.error(e, {
        symbol,
        expiration: expiration,
        strike: option.strike,
      });
    }
  }
}

async function handleOptionUpdatePayload(
  symbol: string,
  expiration: number,
  batchId: Date,
  resp: AxiosResponse<any>
) {
  const result = resp.data["optionChain"]["result"][0];
  const quote = result["quote"];
  const price = midmarket(quote);
  const volume = quote.regularMarketVolume;

  const calls: YahooOptionResponse[] = (result["options"][0][
    "calls"
  ] as YahooOptionResponse[]).map((e) => ({
    ...e,
    type: "call",
  }));
  const puts: YahooOptionResponse[] = (result["options"][0][
    "puts"
  ] as YahooOptionResponse[]).map((e) => ({
    ...e,
    type: "put",
  }));

  const resps: YahooOptionResponse[] = calls.concat(puts);
  updateOptionChain(
    symbol,
    price,
    new Date(expiration * 1000),
    resps,
    batchId,
    volume
  );
}

async function updateOption(symbol: string) {
  const data = await prisma.expiration.findMany({
    where: {
      symbol,
      expiration_date: { gt: dayjs().subtract(1, "day").toDate() },
    },
  });

  console.log(`Got ${data.length} expirations`);
  const batchId = new Date();
  // Done via loop so it's forced to be serial. We have to obey rate limiting here.
  const initial = await axios.get(
    `https://query1.finance.yahoo.com/v7/finance/options/${symbol}`
  );
  const expirations: number[] =
    initial.data["optionChain"]["result"][0]["expirationDates"];
  const firstExpiration = expirations.shift();
  if (firstExpiration) {
    handleOptionUpdatePayload(symbol, firstExpiration, batchId, initial);
  }

  await Promise.all(
    expirations.map(async (expiration: number) => {
      console.log(
        "Updating options chain for",
        symbol,
        new Date(expiration * 1000)
      );

      axios
        .get(
          `https://query1.finance.yahoo.com/v7/finance/options/${symbol}?date=${expiration}`
        )
        .then((resp) => {
          handleOptionUpdatePayload(symbol, expiration, batchId, resp);
        });
    })
  );
  console.log("Done updating");
}

export async function updateHVEstimates(symbol: string, lens: number[]) {
  const maxlen = lens.reduce((m, v) => (m > v ? m : v), 0);
  const results = await Env.tradier.market.getHistoricalPricing(
    symbol,
    TradierHistoryInterval.DAILY,
    dayjs()
      .subtract(maxlen * 2, "day")
      .format("YYYY-MM-DD")
  );
  const allData = results["history"]["day"];

  for (let j = 0; j < lens.length; j++) {
    const len = lens[j];
    for (let i = 0; i < allData.length - len; i++) {
      const data = allData.slice(i, i + len);
      let diffs = [];
      for (let i = 1; i < data.length; i++) {
        diffs.push(Math.log(data[i].close / data[i - 1].close));
      }

      const ending_at = dayjs(data[data.length - 1].date).toDate();
      const volatility = stdDev(diffs) * Math.sqrt(252);

      await Env.prisma.instrumentVolatility.upsert({
        where: {
          symbol_days_ending_at_unique: {
            symbol,
            days: len,
            ending_at,
          },
        },
        update: { volatility },
        create: {
          symbol,
          days: len,
          ending_at,
          volatility,
        },
      });
    }
  }
}
