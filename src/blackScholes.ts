import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";
import timezone from "dayjs/plugin/timezone";
dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault("America/New_York");

function ndist(z: number) {
  return (1.0 / Math.sqrt(2 * Math.PI)) * Math.exp(-0.5 * z);
}

function N(z: number) {
  const b1 = 0.31938153;
  const b2 = -0.356563782;
  const b3 = 1.781477937;
  const b4 = -1.821255978;
  const b5 = 1.330274429;
  const p = 0.2316419;
  const c2 = 0.3989423;
  const a = Math.abs(z);
  if (a > 6.0) {
    return 1.0;
  }
  const t = 1.0 / (1.0 + a * p);
  const b = c2 * Math.exp(-z * (z / 2.0));
  let n = ((((b5 * t + b4) * t + b3) * t + b2) * t + b1) * t;
  n = 1.0 - b * n;
  if (z < 0.0) {
    n = 1.0 - n;
  }
  return n;
}

export function variantToExpiry(
  timeToExpiry: number | Date | string,
  from: Date | string | null = null
) {
  if (typeof timeToExpiry === "number") {
    return timeToExpiry;
  } else {
    return (
      dayjs(timeToExpiry)
        .tz("America/New_York")
        .startOf("day")
        .add(16, "hour")
        .diff((from ? dayjs(from) : dayjs()).tz("America/New_York"), "second") /
      (365 * 86400)
    );
  }
}

export function blackScholes(
  isCall: boolean,
  underlying: number,
  strike: number,
  riskFreeRate: number,
  divdendYield: number = 0,
  volatility: number,
  timeToExpiry: number | Date | string
) {
  const S = underlying;
  const X = strike;
  const r = riskFreeRate;
  const v = volatility;
  const t = variantToExpiry(timeToExpiry);
  const q = divdendYield;

  const sqrtt = Math.sqrt(t);
  const osqrt = v * sqrtt;
  const d1 = (Math.log(S / X) + (r - q + (v * v) / 2) * t) / osqrt;
  const d2 = d1 - osqrt;
  const ert = Math.exp(-r * t);
  const nd1 = ndist(d1);
  let Nd2;
  let delta;

  if (isCall) {
    delta = N(d1);
    Nd2 = N(d2);
  } else {
    delta = -N(-d1);
    Nd2 = -N(-d2);
  }

  const gamma = nd1 / (S * v * sqrtt);
  const vega = S * sqrtt * nd1;
  const theta = (-(S * v * nd1) / (2 * sqrtt) - r * X * ert * Nd2) / 365.0;
  const rho = X * t * ert * Nd2;
  const price = S * delta - X * ert * Nd2;

  return { price, delta, gamma, theta, vega, rho };
}

export function solveIV(
  call: boolean,
  underlying: number,
  strike: number,
  riskFreeRate: number,
  timeToExpiry: number | Date | string,
  optionPrice: number
) {
  const S = underlying;
  const X = strike;
  const r = riskFreeRate;
  const t = variantToExpiry(timeToExpiry);

  if (t < 0) return 0;

  const MAX_ITER = 10000;
  const ACC = 0.01;
  let sigma;
  let bs;

  sigma = 3;
  for (let i = 0; i < MAX_ITER; i++) {
    bs = blackScholes(call, S, X, r, 0, sigma, t);
    const diff = optionPrice - bs.price;
    if (Math.abs(diff) < ACC || bs.vega === 0) return sigma;
    sigma = sigma + diff / (bs.vega * 100);
  }

  // Negative IV isn't possible. Can happen if expiry is in the past, though.
  if (sigma < 0) return 0;
  // We may fail to converge, but we'll take our last best guess
  return sigma;
}
