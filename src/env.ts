import {
  TradierAccountType,
  TradierClient,
  TradierClientOptions,
} from "@reycodev/tradier-client";
import { PrismaClient } from "@prisma/client";

let _prisma: PrismaClient | undefined;
let _tradier: TradierClient | undefined;

let _bucket = 0;
let _requests = 0;

export class Env {
  static get prisma() {
    if (!_prisma) {
      _prisma = new PrismaClient();
    }
    return _prisma;
  }

  static get tradier() {
    if (!_tradier) {
      const options: TradierClientOptions = {
        accessToken: process.env.TRADIER_TOKEN || "", // Token receieved after creating tradier account
        accountType: TradierAccountType.SANDBOX, // Depends on type of account created.
      };

      _tradier = new TradierClient(options);
    }
    return _tradier;
  }

  static get riskFreeRate() {
    return 0.00084;
  }

  static get requests() {
    return _requests;
  }

  static incrementRequest() {
    const minute = Math.round(new Date().getTime() / 60000) * 60000;
    if (minute > _bucket) {
      _bucket = minute;
      _requests = 0;
    }
    _requests += 1;
    console.log("Used", _requests, "requests");
    return _requests;
  }
}
