require("dotenv").config();
import { updateHVEstimates, updateSymbol } from "./poller";
import { Env } from "./env";
import { format } from "util";
import yargs from "yargs";
import config from "./config.js";

import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";
import weekday from "dayjs/plugin/weekday";
import timezone from "dayjs/plugin/timezone";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.extend(weekday);

dayjs.tz.setDefault("America/New_York");

async function updateAllHVEstimates() {
  for (let i = 0; i < config.symbols.length; i++) {
    await updateHVEstimates(config.symbols[i], [7, 14, 30, 180]);
  }
  console.log("Done");
}

async function poll() {
  const wd = dayjs().weekday();
  const isWeekDay = wd >= 1 || wd <= 6;
  const hour = dayjs().utc().hour();
  const afterMarketOpen = hour > 13 || (hour === 13 && dayjs().minute() >= 30);
  const beforeMarketClose = hour < 21;
  if (isWeekDay && afterMarketOpen && beforeMarketClose) {
    for (let i = 0; i < config.symbols.length; i++) {
      const symbol = config.symbols[i];
      await updateSymbol(symbol);
    }
    console.log("Done with poll loop");
  } else {
    console.log("Outside of market hours");
  }

  setTimeout(poll, 75000);
}

async function getNope(symbol: string, update: boolean, useRelative: boolean) {
  if (update) await updateSymbol(symbol);

  let values;
  if (useRelative) {
    values = await Env.prisma.$queryRaw`
    SELECT
      SUM((volume - prev_volume) * delta) * 100 AS netdelta, SUM(volume) AS vol, option_type, avg(underlying_volume) AS underlying_volume
      FROM options
      WHERE
        symbol = ${symbol}
        AND prev_volume != 0
        AND TO_DATE(expiration, 'YYYY-MM-DD') > NOW() - interval '1 day'
      GROUP BY option_type;
  `;
  } else {
    values = await Env.prisma.$queryRaw`
    SELECT
      SUM(volume * delta) * 100 AS netdelta, SUM(volume) AS vol, option_type, avg(underlying_volume) AS underlying_volume
      FROM options
      WHERE
        symbol = ${symbol}
        AND created_at = (select max(created_at) from options where symbol = ${symbol})
        AND TO_DATE(expiration, 'YYYY-MM-DD') > NOW() - interval '1 day'
      GROUP BY option_type;`;
  }

  const cr = values.find((v: any) => v.option_type === "call");
  const pr = values.find((v: any) => v.option_type === "put");

  if (cr && pr) {
    const nope =
      (parseFloat(cr.netdelta) + parseFloat(pr.netdelta)) /
      pr.underlying_volume;
    console.log("Call weighted delta", Math.floor(parseFloat(cr.netdelta)));
    console.log("Call volume", parseInt(cr.vol, 10));
    console.log("Put weighted delta", Math.floor(parseFloat(pr.netdelta)));
    console.log("Put volume", parseInt(pr.vol, 10));
    console.log("Shares volume:", pr.underlying_volume);
    console.log("NOPE:", format("%s%%", Math.floor(nope * 10000) / 100.0));
    await nopeByExpiry(symbol, pr.underlying_volume, useRelative);
  } else {
    console.log("No data!");
  }
}

async function nopeByExpiry(
  symbol: string,
  shares: number,
  useRelative: boolean
) {
  let values;
  if (useRelative) {
    values = await Env.prisma.$queryRaw`
      SELECT
        SUM((volume - prev_volume) * delta) * 100 AS netdelta, expiration, stddev_pop(delta) as delta_std, avg(delta) as delta_avg
        FROM options
        WHERE
          symbol = ${symbol}
          AND prev_volume > 0
          AND TO_DATE(expiration, 'YYYY-MM-DD') > NOW() - interval '1 day'
        GROUP BY expiration
        ORDER BY expiration ASC;
    `;
  } else {
    values = await Env.prisma.$queryRaw`
      SELECT
          SUM(volume * delta) * 100 AS netdelta,
          expiration,
          stddev_pop(delta) as delta_std,
          avg(delta) as delta_avg,
          sum(volume) as volume,
          option_type, avg(delta) - stddev_pop(delta) as delta_low,
          avg(delta) + stddev_pop(delta) as delta_high
        FROM options
        WHERE
          symbol = ${symbol}
          AND created_at = (SELECT max(created_at) FROM options WHERE symbol = ${symbol})
          AND TO_DATE(expiration, 'YYYY-MM-DD') > NOW() - interval '1 day'
        GROUP BY expiration, option_type
        ORDER BY expiration ASC;
        `;
  }

  console.log("------------------------------------------------------------");
  const vals = values.map((row: any) => {
    return {
      option_type: row.option_type,
      expiration: row.expiration,
      nope: format("%s%%", Math.floor((row.netdelta / shares) * 10000) / 100),
      volume: row.volume,
      deltaAvg: Math.round(row.delta_avg * 10000) / 10000,
      deltaStd: Math.round(row.delta_std * 10000) / 10000,
      deltaLow: Math.round(row.delta_low * 10000) / 10000,
      deltaHigh: Math.round(row.delta_high * 10000) / 10000,
      deltaSum: Math.round(row.delta_avg * row.volume * 100) / 100,
    };
  });
  console.table(vals);
}

async function nopeByStrike(
  symbol: string,
  shares: number,
  useRelative: boolean
) {
  let values;
  if (useRelative) {
    values = await Env.prisma.$queryRaw`
      SELECT
        SUM((volume - prev_volume) * delta) * 100 AS netdelta, expiration, stddev_pop(delta) as delta_std, avg(delta) as delta_avg
        FROM options
        WHERE
          symbol = ${symbol}
          AND prev_volume > 0
          AND TO_DATE(expiration, 'YYYY-MM-DD') > NOW() - interval '1 day'
        GROUP BY strike
        ORDER BY strike ASC;
    `;
  } else {
    values = await Env.prisma.$queryRaw`
      SELECT
          SUM(volume * delta) * 100 AS netdelta,
          expiration,
          stddev_pop(delta) as delta_std,
          avg(delta) as delta_avg,
          sum(volume) as volume,
          option_type, avg(delta) - stddev_pop(delta) as delta_low,
          avg(delta) + stddev_pop(delta) as delta_high
        FROM options
        WHERE
          symbol = ${symbol}
          AND created_at = (SELECT max(created_at) FROM options WHERE symbol = ${symbol})
          AND TO_DATE(expiration, 'YYYY-MM-DD') > NOW() - interval '1 day'
        GROUP BY expiration, option_type
        ORDER BY expiration ASC;
        `;
  }

  console.log("------------------------------------------------------------");
  const vals = values.map((row: any) => {
    return {
      option_type: row.option_type,
      expiration: row.expiration,
      nope: format("%s%%", Math.floor((row.netdelta / shares) * 10000) / 100),
      volume: row.volume,
      deltaAvg: Math.round(row.delta_avg * 10000) / 10000,
      deltaStd: Math.round(row.delta_std * 10000) / 10000,
      deltaLow: Math.round(row.delta_low * 10000) / 10000,
      deltaHigh: Math.round(row.delta_high * 10000) / 10000,
      deltaSum: Math.round(row.delta_avg * row.volume * 100) / 100,
    };
  });
  console.table(vals);
}

const args = yargs
  .command(
    "nope",
    "Update options chains and print NOPE for a ticker",
    (yargs) => {
      return yargs
        .option("symbol", {
          alias: "s",
        })
        .option("update", {
          alias: "u",
          type: "boolean",
        })
        .option("relative", {
          alias: "r",
          type: "boolean",
        });
    }
  )
  .command("poll", "Poll options chains")
  .command("hv", "Estimate historical volatility", (yargs) => {
    return yargs.option("symbol", {
      alias: "s",
    });
  })
  .help()
  .alias("h", "help").argv;

async function main() {
  switch (args._[0]) {
    case "nope":
      await getNope(
        args.symbol as string,
        args.update as boolean,
        args.relative as boolean
      );
      break;
    case "poll":
      await poll();
      break;
    case "hv":
      await updateAllHVEstimates();
      break;
    default:
      yargs.showHelp();
  }
}

main()
  .catch((e) => {
    throw e;
  })
  .finally(async () => {
    await Env.prisma.$disconnect();
  });
