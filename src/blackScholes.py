import math


def ndist(z):
    return (1.0 / math.sqrt(2 * math.pi)) * math.exp(-0.5 * z)


def N(z):
    b1 = 0.31938153
    b2 = -0.356563782
    b3 = 1.781477937
    b4 = -1.821255978
    b5 = 1.330274429
    p = 0.2316419
    c2 = 0.3989423
    a = abs(z)
    if (a > 6.0):
        return 1.0
    t = 1.0 / (1.0 + a * p)
    b = c2 * math.exp(-z * (z / 2.0))
    n = ((((b5 * t + b4) * t + b3) * t + b2) * t + b1) * t
    n = 1.0 - b * n
    if z < 0.0:
        n = 1.0 - n
    return n


def blackScholes(isCall, underlying, strike, riskFreeRate, divdendYield, volatility, timeToExpiry):
    S = underlying
    X = strike
    r = riskFreeRate
    v = volatility
    t = timeToExpiry
    q = divdendYield

    sqrtt = math.sqrt(t)
    osqrt = v * sqrtt
    d1 = (math.log(S / X) + (r - q + (v * v) / 2) * t) / osqrt
    d2 = d1 - osqrt
    ert = math.exp(-r * t)
    nd1 = ndist(d1)

    if isCall:
        delta = N(d1)
        Nd2 = N(d2)
    else:
        delta = -N(-d1)
        Nd2 = -N(-d2)

    gamma = nd1 / (S * v * sqrtt)
    vega = S * sqrtt * nd1
    theta = (-(S * v * nd1) / (2 * sqrtt) - r * X * ert * Nd2) / 365.0
    rho = X * t * ert * Nd2
    price = S * delta - X * ert * Nd2

    return price, delta, gamma, theta, vega, rho


def solveIV(call, underlying, strike, riskFreeRate, timeToExpiry, optionPrice, divYield=0):
    S = underlying
    X = strike
    r = riskFreeRate
    t = timeToExpiry
    q = divYield

    if (t < 0):
        return 0

    MAX_ITER = 10000
    ACC = 0.01

    sigma = 3
    for i in range(0, MAX_ITER):
        bs = blackScholes(call, S, X, r, q, sigma, t)
        diff = optionPrice - bs[0]
        if abs(diff) < ACC or bs[4] == 0:
            return sigma
        sigma = sigma + diff / (bs[4] * 100)

    # Negative IV isn't possible. Can happen if expiry is in the past, though.
    if (sigma < 0):
        return 0
    # We may fail to converge, but we'll take our last best guess
    return sigma


if __name__ == "__main__":
    print(blackScholes(True, 115, 115, 0.01, 0, 0.5, 7.0 / 365.0))
    print(solveIV(True, 115, 115, 0.01, 7.0 / 365.0, 1.2))
