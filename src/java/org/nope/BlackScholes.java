import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

class BlackScholes {
    final int MAX_ITER = 10000;
    final double ACC = 0.01;

    public double delta;
    public double gamma;
    public double theta;
    public double vega;
    public double rho;
    public double price;

    private boolean isCall;
    private double underlying;
    private double strike;
    private double riskFreeRate;
    private double dividendYield;
    private double timeToExpiry;

    /**
     * Compute the fair price and greeks for an options contract
     *
     * @param isCall        True if call, false if put
     * @param underlying    The spot price of the underlying instrument
     * @param strike        The strike price of the option
     * @param riskFreeRate  Risk free interest rate, usually the yield of a Treasury
     *                      of roughly the same duration as the contract, less
     *                      inflation. I've been using 0.01.
     * @param dividendYield Annual dividend yield of the underlying. Use 0 if
     *                      unsure.
     * @param timeToExpiry  Time until the contract expires, in years.
     */

    BlackScholes(boolean isCall, double underlying, double strike, double riskFreeRate, double dividendYield,
            double timeToExpiry) {
        this.isCall = isCall;
        this.underlying = underlying;
        this.strike = strike;
        this.riskFreeRate = riskFreeRate;
        this.dividendYield = dividendYield;
        this.timeToExpiry = timeToExpiry;
    }

    /**
     * Compute the fair price and greeks for an options contract
     *
     * @param volatility Implied or historical volatility
     */
    public double compute(double volatility) {
        double S = this.underlying;
        double X = this.strike;
        double r = this.riskFreeRate;
        double t = this.timeToExpiry;
        double q = this.dividendYield;
        double v = volatility;

        double sqrtt = Math.sqrt(t);
        double osqrt = v * sqrtt;
        double d1 = (Math.log(S / X) + (r - q + (v * v) / 2) * t) / osqrt;
        double d2 = d1 - osqrt;
        double ert = Math.exp(-r * t);
        double nd1 = ndist(d1);
        double Nd2;

        if (isCall) {
            delta = N(d1);
            Nd2 = N(d2);
        } else {
            delta = -N(-d1);
            Nd2 = -N(-d2);
        }

        this.gamma = nd1 / (S * v * sqrtt);
        this.vega = S * sqrtt * nd1;
        this.theta = (-((S * v * nd1) / (2 * sqrtt)) - (r * X * ert * Nd2)) / 365;
        this.rho = X * t * ert * Nd2;
        this.price = S * delta - X * ert * Nd2;

        return price;
    }

    public double solveIV(double optionPrice) {
        double t = timeToExpiry;

        if (t < 0)
            return 0;
        double sigma;

        sigma = 3;
        for (int i = 0; i < MAX_ITER; i++) {
            double price = this.compute(sigma);
            double diff = optionPrice - price;
            if (Math.abs(diff) < ACC || vega == 0)
                return sigma;
            sigma = sigma + diff / (vega * 100);
        }

        // Negative IV isn't possible. Can happen if expiry is in the past, though.
        if (sigma < 0)
            return 0;
        // We may fail to converge, but we'll take our last best guess
        return sigma;
    }

    static final ZoneId DEST_ZONE = ZoneId.of("America/New_York");
    static double timeToExpiry(ZonedDateTime from, ZonedDateTime to) {
        long diffInSeconds = Math.abs(to.toEpochSecond() - from.toEpochSecond());
        return ((double)diffInSeconds) / (365d * 86400d);
    }

    private double N(double z) {
        double b1 = 0.31938153;
        double b2 = -0.356563782;
        double b3 = 1.781477937;
        double b4 = -1.821255978;
        double b5 = 1.330274429;
        double p = 0.2316419;
        double c2 = 0.3989423;
        double a = Math.abs(z);
        if (a > 6.0) {
            return 1.0;
        }
        double t = 1.0 / (1.0 + a * p);
        double b = c2 * Math.exp(-z * (z / 2.0));
        double n = ((((b5 * t + b4) * t + b3) * t + b2) * t + b1) * t;
        n = 1.0 - b * n;
        if (z < 0.0) {
            n = 1.0 - n;
        }
        return n;
    }

    private double ndist(double z) {
        return (1.0 / Math.sqrt(2 * Math.PI)) * Math.exp(-0.5 * z);
    }
}
