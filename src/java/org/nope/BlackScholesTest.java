import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.junit.Test;
import org.junit.Assert;

public class BlackScholesTest {
    @Test()
    public void testCompute() {
        BlackScholes bs = new BlackScholes(true, 100, 105, 0.01, 0, 5 / 365.0);
        double price = bs.compute(0.5);
        Assert.assertEquals(0.6815848151769792, price, 0.0001);
    }

    @Test()
    public void testSolveIV() {
        BlackScholes bs = new BlackScholes(true, 100, 105, 0.01, 0, 5 / 365.0);
        double iv = bs.solveIV(0.75);
        Assert.assertEquals(0.5228132661117652, iv, 0.0001);
    }

    @Test()
    public void testGreeks() {
        BlackScholes bs = new BlackScholes(true, 115, 115, 0.01, 0, 7.0 / 365.0);

        double iv = bs.solveIV(3.48);
        double price = bs.compute(iv);

        Assert.assertEquals(0.516135, bs.delta, 0.0001);
        Assert.assertEquals(0.044816, bs.gamma, 0.0001);
        Assert.assertEquals(-0.24513, bs.theta, 0.0001);
        Assert.assertEquals(6.2262367, bs.vega, 0.0001);
        Assert.assertEquals(1.0713942, bs.rho, 0.0001);
    }

    @Test()
    public void timeToExpiry() {
        double r = BlackScholes.timeToExpiry(
            LocalDate.parse("2020-10-23").atTime(0, 0).atZone(ZoneId.of("America/Phoenix")),
            LocalDate.parse("2020-10-30").atTime(16, 0).atZone(ZoneId.of("America/New_York"))
        );
        Assert.assertEquals(0.02066210, r, 0.0001);
    }
}
