import { describe } from "mocha";
import { expect } from "chai";
import { blackScholes, solveIV, variantToExpiry } from "./blackScholes";
import dayjs from "dayjs";

describe("solveIV", function () {
  it("converges", function () {
    const result = solveIV(true, 346.165, 310, 0.01, 0.05, 40);
    expect(result).to.equal(0.5322121884417123);
  });
});

describe("variantToExpiry", function () {
  console.log(variantToExpiry("2020-10-23") * 365 * 24);
});

describe("blackScholes", function () {
  const t = [];
  for (let i = 0; i <= 10; i++) {
    const r1 = blackScholes(
      true,
      49.855,
      50,
      0.01,
      0,
      0.027069,
      ((i / 10) * 3) / 365
    );
    t.push({ dte: (i / 10) * 3, delta: r1.delta });
  }
  console.table(t);
});
