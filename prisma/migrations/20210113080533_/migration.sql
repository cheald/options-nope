-- CreateTable
CREATE TABLE "instruments" (
    "id" text NOT NULL,
    "symbol" text NOT NULL,
    "expirations_updated_at" timestamp(3) NOT NULL,
    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "instrument_volatilities" (
    "id" text NOT NULL,
    "symbol" text NOT NULL,
    "days" integer NOT NULL,
    "ending_at" timestamp(3) NOT NULL,
    "volatility" DECIMAL(65, 30) NOT NULL,
    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "expirations" (
    "id" text NOT NULL,
    "symbol" text NOT NULL,
    "expiration" text NOT NULL,
    "expiration_date" timestamp(3) NOT NULL,
    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "options" (
    "id" text NOT NULL,
    "symbol" text NOT NULL,
    "expiration" text NOT NULL,
    "expiration_date" timestamp(3) NOT NULL,
    "strike" DECIMAL(65, 30),
    "spot" DECIMAL(65, 30) NOT NULL,
    "option_type" text NOT NULL,
    "name" text NOT NULL,
    "created_at" timestamp(3) NOT NULL,
    "last" DECIMAL(65, 30),
    "change" DECIMAL(65, 30),
    "volume" integer NOT NULL,
    "bid" DECIMAL(65, 30),
    "ask" DECIMAL(65, 30),
    "prev_volume" integer NOT NULL,
    "underlying_volume" integer NOT NULL,
    "last_volume" integer,
    "prev_close" DECIMAL(65, 30),
    "bid_size" integer,
    "ask_size" integer,
    "open_interest" integer,
    "gamma" DECIMAL(65, 30),
    "delta" DECIMAL(65, 30),
    "theta" DECIMAL(65, 30),
    "rho" DECIMAL(65, 30),
    "vega" DECIMAL(65, 30),
    "implied_volatility" DECIMAL(65, 30),
    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "instruments.symbol_unique" ON "instruments" ("symbol");

-- CreateIndex
CREATE UNIQUE INDEX "instrument_volatilities.symbol_days_ending_at_unique" ON "instrument_volatilities" ("symbol", "days", "ending_at");

-- CreateIndex
CREATE UNIQUE INDEX "expirations.symbol_expiration_unique" ON "expirations" ("symbol", "expiration");

-- CreateIndex
CREATE INDEX "expirations.symbol_expiration_date_index" ON "expirations" ("symbol", "expiration_date");

-- CreateIndex
CREATE INDEX "options.name_index" ON "options" ("name");

-- CreateIndex
CREATE INDEX "options.sym_ts_idx" ON "options" ("symbol", "created_at");

-- CreateIndex
CREATE INDEX "options.symbol_expiration_strike_option_type_index" ON "options" ("symbol", "expiration", "strike", "option_type");
