-- AlterIndex
ALTER INDEX "expirations.symbol_expiration_date_index" RENAME TO "symbol_expiration_date_index";

-- AlterIndex
ALTER INDEX "expirations.symbol_expiration_unique" RENAME TO "symbol_expiration_unique";

-- AlterIndex
ALTER INDEX "instrument_volatilities.symbol_days_ending_at_unique" RENAME TO "symbol_days_ending_at_unique";

-- AlterIndex
ALTER INDEX "options.name_index" RENAME TO "name_index";

-- AlterIndex
ALTER INDEX "options.sym_ts_idx" RENAME TO "sym_ts_idx";

-- AlterIndex
ALTER INDEX "options.symbol_expiration_strike_option_type_index" RENAME TO "symbol_expiration_strike_option_type_index";
