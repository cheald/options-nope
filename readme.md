1. Create a .env file containing

   TRADIER_TOKEN=YOUR_TOKEN_HERE

2. Create a postgres role named `options` with password `options`, and a DB for it:

   createuser -P options
   createdb options -O options

3. Install packages

   yarn

4. Migrate the DB

   yarn prisma migrate up --experimental

5. Invoke the typescript compiler to compile the project

   tsc

6. Run it!

   yarn nope -s SPY -u
